#! /usr/bin/env python
#
# *
# *  This program is free software; you can redistribute it and/or
# *  modify it under the terms of the GNU Library General Public License
# *  as published by the Free Software Foundation; either version 2 of
# *  the License, or (at your option) any later version.
# *
# *  This program is distributed in the hope that it will be useful,
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *  GNU Library General Public License for more details.
# *
# *  You should have received a copy of the GNU Library General Public
# *  License along with this program; if not, write to the Free Software
# *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# *
# */

import pygtk; pygtk.require("2.0")
import gda

def main ():

    client = gda.Client();

    #TODO: This should probably be config_get_provider_list()
    providers = gda.config_get_provider_list();

    print "Number of providers = ", len(providers);

    # Print the information about each Provider:
    for info in providers: 
        print "Provider: ID = ", info.id, "\n";
        # TODO: Wrap these public struct fields:
        #"  location = ", info.location, "\n",
        #"  description = ", info.description;

    # The params:
    # print "  GDA Params: ";

    # TODO: Wrap this public struct field:
    # params = info.get_gda_params();
    # for param in params: 
    #    print param, ", ";

    print "\n\n";

    data_sources = gda.config_get_data_source_list();

    print "Number of data sources = ", len(data_sources), "\n\n";

    # Print the information about each data source:
    for info in data_sources:
        print "Data source: name = ", info.name, "\n", 
        "  provider = ", info.provider, "\n", 
        "  connection string = ", info.cnc_string, "\n", 
        "  description = ", info.description, "\n", 
        "  username = ", info.username, "\n\n";



main ()

