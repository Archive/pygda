#! /usr/bin/env python
#
# *
# *  This program is free software; you can redistribute it and/or
# *  modify it under the terms of the GNU Library General Public License
# *  as published by the Free Software Foundation; either version 2 of
# *  the License, or (at your option) any later version.
# *
# *  This program is distributed in the hope that it will be useful,
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *  GNU Library General Public License for more details.
# *
# *  You should have received a copy of the GNU Library General Public
# *  License along with this program; if not, write to the Free Software
# *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# *
# */

from gi.repository import Gda

def main ():

    data_source_name = "datasource_pygda_example_select11";

    #TODO: This should be Gda.Config.find_data_source().
    gdaconfig = Gda.Config.get();
    data_source = gdaconfig.get_dsn_info(data_source_name);
    if not data_source:
        print "Creating the DataSource, because it does not exist yet.";

        # Create it if it does not exist already:
        data_source = Gda.DsnInfo();
        data_source.name = data_source_name;
        data_source.username = "murrayc";
        data_source.password = "murraycpw";
        data_source.description = "Data Source for libgdamm simple example.";
        data_source.provider = "PostgreSQL";
        # You must specify a database when using PostgreSQL, even when you want to create a database.
        # template1 always exists with PostgreSQL.
        # data_source.cnc_string = "DB_NAME=template1";
        data_source.cnc_string = "DB_NAME=glom_example_smallbusiness_v114;HOST=localhost";

        # TODO: Add save_data_source(data_source_info);
        Gda.config_save_data_source(data_source.name, data_source.provider, data_source.cnc_string, data_source.description, data_source.username, data_source.password, False);

    # Note that, if the server is not running, this can take a few minutes to fail: */
    print "DEBUG: Before opening connection\n"
    gda_connection = Gda.open_connection(data_source.name, data_source.username, data_source.password);
    print "DEBUG: After opening connection\n"

    if not gda_connection:
        print "Error: Could not open connection to ", data_source.name;
        return;

    # Open database:
    # This does not work with PostgreSQL: gda_connection.change_database("murrayc");

    # Get data from a table:
    command = Gda.Command("SELECT * FROM contacts");

    print "DEBUG: Before executing command\n"
    data_model = gda_connection.execute_select_command(command);
    print "DEBUG: After executing command\n"

    if not data_model:
        print "command execution failed.";

    rows = data_model.get_n_rows();
    columns = data_model.get_n_columns();
    print "    Number of columns: ", columns;

    for i in range(columns):
        print "      column ", i;
        print "        name=", data_model.get_column_title(i);

        # Find out whether it's the primary key:
        field = data_model.describe_column(i);
        if field.get_primary_key(): #TODO: This should probably be wrapped as field.primary_key.
            print "        (primary key)";

        print "\n";

    print "    Number of rows: ", rows;

    for row_index in range(rows):
        print "      row ", row_index;

        for col_index in range(columns):
          #TODO: Implement automatic conversion from the GdaValue to the correct basic python type.
          print "        value=", data_model.get_value_at(col_index, row_index);

        print "\n";

main ()
